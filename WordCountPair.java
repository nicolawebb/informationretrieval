/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puoject;


/**
 *
 * @author meric
 */
public class WordCountPair implements Comparable<WordCountPair>{

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }
 
    private String word;
    private int count;
    
    public WordCountPair(String wordToAdd){
    word = wordToAdd;
    count = 1;
    }
    
    public WordCountPair(String wordToAdd,int count){
    word = wordToAdd;
    count = count;
    }
    
    public void increaseCount(){
        setCount(getCount() + 1);
    }

    @Override
    public int compareTo(WordCountPair o) {
        
        Integer thisCount = new Integer(this.getCount());
        Integer otherCount = new Integer(o.getCount());
      return -(thisCount.compareTo(otherCount));
    }
    
    
    
}
