/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puoject;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author meric
 */
public class NewsCrawler implements Runnable {

    String urlToCrawl;
    int waitTime;

    public NewsCrawler(String urll,int waitInSecs) {
        this.urlToCrawl = urll;
        waitTime = waitInSecs;
        System.out.println(urll);
    }

    public Document Crawl() {

        try {
            Connection connecto = Jsoup.connect(urlToCrawl);
            // dang special
            //connecto.maxBodySize(0).timeout(0).get().html();
            String htmlPage = connecto.get().html();
            Document doc = Jsoup.parse(htmlPage);
            Suughp.theNewsFeed.addDocument(doc);

            return doc;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void run() {

        try {
            while (true) {
//                System.out.println("Crawling .." + urlToCrawl);
                Crawl();
                Thread.sleep(waitTime);
                
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(NewsCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
