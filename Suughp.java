package puoject;

/**
 *
 * @author meric
 */
public class Suughp {

    public static NewsFeed theNewsFeed = new NewsFeed();
    public static final String ourFormat = "EEE, dd MMM yyyy HH:mm:ss zzz";
    public static String theQueryText;
    public static void main(String args[]) throws InterruptedException {

        SearchUI newUi = new  SearchUI();

        NewsCrawler siliconValley = new NewsCrawler("https://www.voanews.com/api/zyritequir", 5000);
        NewsCrawler science = new NewsCrawler("https://www.voanews.com/api/zt$opeitim", 2000);
        NewsCrawler artAndCulture = new NewsCrawler("https://www.voanews.com/api/zp$ove-vir", 3000);
        NewsCrawler europe = new NewsCrawler("https://www.voanews.com/api/zj$oveytit", 7000);
        NewsCrawler usa = new NewsCrawler("https://www.voanews.com/api/zq$omekvi_", 1000);

        DocumentProcessor d1 = new DocumentProcessor(1000);
        DocumentProcessor d2 = new DocumentProcessor(1000);
        DocumentProcessor d3 = new DocumentProcessor(1000);
        DocumentProcessor d4 = new DocumentProcessor(1000);
        DocumentProcessor d5 = new DocumentProcessor(1000);

        Thread t1 = new Thread(siliconValley);
        Thread t2 = new Thread(science);
        Thread t3 = new Thread(artAndCulture);
        Thread t4 = new Thread(europe);
        Thread t5 = new Thread(usa);
        Thread t6 = new Thread(d1);
        Thread t7 = new Thread(d2);
        Thread t8 = new Thread(d3);
        Thread t9 = new Thread(d4);
        Thread t10 = new Thread(d5);
        Thread t11 = new Thread(newUi);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();
        t11.start();
        
        System.out.println("Document pool size " + theNewsFeed.docs.size());
        System.out.println("News Feed size " + theNewsFeed.feedSize());

    }

}
