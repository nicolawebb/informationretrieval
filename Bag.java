package puoject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.StringTokenizer;
import javafx.util.Pair;

/**
 *
 * @author meric
 */
public class Bag{

    ArrayList<WordCountPair> theBag = new ArrayList<>();

    public int containsToken(String token) {

        if (theBag.size() > 0) {
            for (int i = 0; i < theBag.size(); i++) {

                String comparedWord = theBag.get(i).getWord();
                
                if (comparedWord!= null && comparedWord.equals(token)) {
                    return i;
                }
            }
        }

        return -1;

    }

    public void addPair(String token) {

        int index;
        index = containsToken(token);
        if (index != -1) {
            theBag.get(index).increaseCount();
        } else {
            theBag.add(new WordCountPair(token));
        }

    }

    public Bag() {
        theBag = new ArrayList();

    }

    public Bag(Article art) {
        theBag = new ArrayList();
        Bagify(art);
        Collections.sort(theBag);
    }
    
    public Bag(String str){
        theBag = new ArrayList();
        Bagify(str);
        Collections.sort(theBag);
    }
    
    
    public void Bagify(Article anArticle) {

        String article = anArticle.getTitle()+" "+anArticle.getDescription();
        String articleWithoutBS = article.replaceAll("[^a-zA-Z ]", "").toLowerCase(Locale.UK);
       // System.out.println(articleWithoutBS);
        int index;
//       StringTokenizer multiTokenizer = new StringTokenizer(article, "://.-    ");
        StringTokenizer multiTokenizer = new StringTokenizer(articleWithoutBS, "    ");

        while (multiTokenizer.hasMoreTokens()) {
            String curToken = multiTokenizer.nextToken().trim();
            if (!curToken.equals("") && curToken != null && !StopWords.isStopword(curToken)) {
                index = containsToken(curToken);
                if (index != -1) {
                    theBag.get(index).increaseCount();
                } else {
                    addPair(curToken);
                }
            }

        }
    }
    
    public void Bagify(String str){
    
        String queryWithoutBS = str.replaceAll("[^a-zA-Z ]", "").toLowerCase(Locale.UK);
        int index;
        StringTokenizer multiTokenizer = new StringTokenizer(queryWithoutBS, "    ");

        while (multiTokenizer.hasMoreTokens()) {
            String curToken = multiTokenizer.nextToken().trim();
            if (!curToken.equals("") && curToken != null && !StopWords.isStopword(curToken)) {
                index = containsToken(curToken);
                if (index != -1) {
                    theBag.get(index).increaseCount();
                } else {
                    addPair(curToken);
                }
            }

        }
    }
    
    public double compareTo(Bag abag){
    double similarityScore=0;
    
    for(int i = 0; i<abag.theBag.size();i++){
       int indexOfContainedElement = this.containsToken(abag.theBag.get(i).getWord());
        if(indexOfContainedElement != -1){
           int occuranceCount = this.theBag.get(indexOfContainedElement).getCount();
           
           if(occuranceCount > 1){
            similarityScore += 1+ Math.log(occuranceCount-1);
           }
           else{
           similarityScore += occuranceCount;
           }
        
        }
    }
    
    return similarityScore;
    }
    

    
    
    @Override
    public String toString(){
        String outStr = "";
        for(WordCountPair wcp : theBag){
            outStr += "["+wcp.getWord() + "("+wcp.getCount() +")]" ;
        }
        //outStr = outStr+"\n";
        return outStr;
    }
    
    
}
