/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puoject;

/**
 *
 * @author meric
 */
public class ArticleRelevancyPair implements Comparable<ArticleRelevancyPair>{

    /**
     * @return the theArticle
     */
    public Article getTheArticle() {
        return theArticle;
    }

    /**
     * @param theArticle the theArticle to set
     */
    public void setTheArticle(Article theArticle) {
        this.theArticle = theArticle;
    }

    /**
     * @return the relevancyScore
     */
    public double getRelevancyScore() {
        return relevancyScore;
    }

    /**
     * @param relevancyScore the relevancyScore to set
     */
    public void setRelevancyScore(double relevancyScore) {
        this.relevancyScore = relevancyScore;
    }

    private Article theArticle;
    private double relevancyScore;
    
    public ArticleRelevancyPair(Article art,double  score){
    theArticle = art;
    relevancyScore = score;
    }
    
    
    
    @Override
    public int compareTo(ArticleRelevancyPair o) {
        
       Double thisScore  = new Double(this.relevancyScore);
       Double otherScore = new Double(o.getRelevancyScore());

       return -(thisScore.compareTo(otherScore));
    
    }
 
    @Override
    public String toString(){
        return theArticle.toString() + " score " + relevancyScore;
    }
    
}
