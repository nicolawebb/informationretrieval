package puoject;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class DocumentProcessor implements Runnable {

    int waitTime;

    public DocumentProcessor(int waitInSecs) {
        waitTime = waitInSecs;
    }

    public void ProcessDocument() {

        Document doc = Suughp.theNewsFeed.popDocument();

        if (doc != null) {
            for (Element item : doc.getElementsByTag("item")) {
                //System.out.println(item.toString());
                Suughp.theNewsFeed.addNewArticle(new Article(item));
            }
        }

    }

    @Override
    public void run() {
        try {
            while (true) {
//                System.out.println("Proccessing document...");
                Thread.sleep(waitTime);
                ProcessDocument();
//                System.out.println("Article count" + Suughp.theNewsFeed.feedSize());
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(DocumentProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
