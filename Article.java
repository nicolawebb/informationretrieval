package puoject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.jsoup.nodes.Element;

/**
 *
 * @author meric
 */
public class Article {

    /**
     * @return the datePublished
     */
    public Date getDatePublished() {
        return datePublished;
    }

    /**
     * @param datePublished the datePublished to set
     */
    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    private String title;
    private String description;
    private String url;
    private Date datePublished;
    public Bag articleBag;

    public Article(String title, String description, String url, Date pubDate) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.datePublished = pubDate;

    }
    
    @Override
    public String toString(){
    String outStr ="["+ title +"]["+ getDatePublished().toString() +"]\n["+ description+"]\n["+url +"]\nBag Of Words["+articleBag.toString() +"]\n" ;
    return outStr;
    }
    

    public Article(Element element) {

        try {

            String atitle = element.getElementsByTag("title").toString().replaceAll("<title>", "").replaceAll("</title>", "").trim();
            String adescription = element.getElementsByTag("description").toString().replaceAll("<description>", "").replaceAll("</description>", "").replaceAll("nbsp", "").trim();
            String alink = element.getElementsByTag("guid").toString().replaceAll("<guid>", "").replaceAll("</guid>", "").trim();
            String apubDate = element.getElementsByTag("pubDate").toString().replaceAll("<pubdate>", "").replaceAll("</pubdate>", "").trim();
            // date format here is Wed, 28 Nov 2018 12:47:40 -0500
            DateFormat dateFormater = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
            Date date = dateFormater.parse(apubDate);

            this.title = atitle;
            this.description = adescription;
            this.url = alink;
            this.datePublished = date;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.articleBag = new Bag(this);

    }

    public Article ElementToArticle(Element element) {

        try {

            String atitle = element.getElementsByTag("title").toString().replaceAll("<title>", "").replaceAll("</title>", "");
            String adescription = element.getElementsByTag("description").toString().replaceAll("<description>", "").replaceAll("</description>", "").replaceAll("nbsp", "");
            String alink = element.getElementsByTag("guid").toString().replaceAll("<guid>", "").replaceAll("</guid>", "");
            String apubDate = element.getElementsByTag("pubDate").toString().replaceAll("<pubdate>", "").replaceAll("</pubdate>", "").trim();
            // date format here is Wed, 28 Nov 2018 12:47:40 -0500
            DateFormat dateFormater = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
            Date date = dateFormater.parse(apubDate);
            Article aNew = new Article(atitle, adescription, alink, date);

            return aNew;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

}
