/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puoject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Stack;
import org.jsoup.nodes.Document;

/**
 *
 * @author meric
 */
public class NewsFeed {

    public Stack<Document> docs;
    public ArrayList<Article> feed;

    public NewsFeed() {
        feed = new ArrayList<>();
        docs = new Stack<>();
    }

    public synchronized void  addNewArticle(Article aArticle) {
        boolean found = false;

        if (feed.size() > 0) {
            for (Article art : feed) {
                if (aArticle.getTitle().equals(art.getTitle())) {
                    found = true;
                }
            }
            if (!found) {
                feed.add(aArticle);
            }
        } else {
            feed.add(aArticle);
        }
    }
    
    public synchronized ArrayList<ArticleRelevancyPair> queryFeed(Query que){
    
        
        ArrayList<ArticleRelevancyPair> relevantArticles = new ArrayList<>();
        
        System.out.println("The query as a bag " + que.queryAsABag.toString());
        
        for(int i=0;i<feed.size();i++){
           double similarity=0; 
           
           
           similarity = feed.get(i).articleBag.compareTo(que.queryAsABag);
//           similarity = que.queryAsABag.compareTo(feed.get(i).articleBag);
            if(similarity > 0){
                relevantArticles.add(new ArticleRelevancyPair(feed.get(i),similarity));
            }
        }
        if(relevantArticles.size() > 0){
            Collections.sort(relevantArticles);
        }
        
        return relevantArticles;
    }
    
    

    public int feedSize() {
        return feed.size();
    }

    public void addDocument(Document aDoc) {
        docs.push(aDoc);
    }

    public synchronized Document  popDocument() {
        if (docs.size() > 0) {
            return docs.pop();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        String outStr = "";
        for (Article art : feed) {
            outStr += art.toString() + "\n";
        }

        return outStr;
    }

}
